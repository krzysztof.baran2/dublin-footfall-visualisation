# Dublin Footfall Visualisation

* For this project, I was tasked to present an interesting phenomenon visible in the [Dublin footfall dataset](https://data.smartdublin.ie/dataset/dublin-city-centre-footfall-counters).

* I designed a dot plot that portrays the footfall count of Dublin streets in 2020 (pre-pandemic) and 2021. 

* I wanted to see whether the pandemic had any effect on the footfall across Dublin.

* Indeed, the footfall count had clearly decreased since the Covid pandemic began.

* This graph shows the quarterly values for both years for each street. (data is incomplete for 4th quarter 2021)

* In this project you may find:

	1. [Four quarterly graphs](Quarterly Charts.pdf)
	2. [My presentation of the visualisation](Visualisation Assignment Presentation with Recording.pptx)
	3. [The colab notebook on which I coded the visualisation.](A5_Code_and_Work_Notebook.ipynb)

* The colab notebook has an element of interactivity where the viewer decides which quarter graph to view.
